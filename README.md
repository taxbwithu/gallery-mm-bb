# Gallery
Showing pictures, using unsplash API


#### Dependencies

- rxjava2
- navigation component
- daggerhilt
- retrofit + gson
- Glide
- Paging 3